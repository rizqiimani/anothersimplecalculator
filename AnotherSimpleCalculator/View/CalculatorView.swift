//
//  CalculatorView.swift
//  SimpleCalculator
//
//  Created by Muhammad Rizqi Imani on 08/05/20.
//  Copyright © 2020 Qatros. All rights reserved.
//

import UIKit

class CalculatorView: UIViewController {
    @IBOutlet weak var calculatorDisplay: UITextView!
    
    var presenter : CalculatorPresenter!
    
    init() {
        super.init(nibName: nil, bundle: nil)
        presenter = CalculatorPresenter(view: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func btnClearPressed(_ sender: Any) {
        presenter.clearPressed()
    }
    
    @IBAction func btnNumberPressed(_ sender: UIButton) {
        presenter.numberPressed(number: sender.currentTitle ?? "")
    }
    
    @IBAction func btnOperatorPressed(_ sender: UIButton) {
        presenter.operandPressed(operand: sender.currentTitle ?? "")
    }
    @IBAction func btnEqualPressed(_ sender: UIButton) {
        presenter.equalPressed()
    }
    
    
}

extension CalculatorView : PresenterToView {
    func setPressedOperand() {
        calculatorDisplay.text = ""
    }
    
    func setPressedNumber(number: String) {
        calculatorDisplay.text = number
    }
    
    func cleared() {
        calculatorDisplay.text = "0"
    }
    
    func giveResult(result: String) {
        calculatorDisplay.text = result
    }
    
}
