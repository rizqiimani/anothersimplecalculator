//
//  CalculatorPresenter.swift
//  AnotherSimpleCalculator
//
//  Created by Muhammad Rizqi Imani on 19/05/20.
//  Copyright © 2020 Qatros. All rights reserved.
//

import UIKit

class CalculatorPresenter {
    var view : PresenterToView
    var defaultNumber = 0
    var numberToDisplay = "0"
    var firstNumber = 0
    var secondNumber = 0
    var setOperand = ""
    var isPressingNumber = false
    
    init(view : CalculatorView) {
        self.view = view
    }
}

extension CalculatorPresenter : ViewToPresenter {
    func numberPressed(number: String) {
        if isPressingNumber {
            numberToDisplay += number
        } else {
            numberToDisplay = number
            isPressingNumber = true
        }
        view.setPressedNumber(number: isPressingNumber ? numberToDisplay : number) // Ternary Operator
        
        //        if numberToDisplay == 0 {
        //            numberToDisplay = number
        //            view.setPressedNumber(number: numberToDisplay)
        //        } else {
        //            numberToDisplay = Int("\(numberToDisplay)\(number)") ?? 0
        //            view.setPressedNumber(number: numberToDisplay)
        //        }
        // Untuk pembelajaran, atau revert back kalau yg testing error.
    }
    func clearPressed() {
        isPressingNumber = false
        numberToDisplay = "0"
        firstNumber = 0
        secondNumber = 0
        view.cleared()
    }
    func operandPressed(operand: String) {
        isPressingNumber = false
        firstNumber = Int(numberToDisplay) ?? 0
        setOperand = operand
        numberToDisplay = ""
        view.setPressedOperand()
    }
    
    func equalPressed() {
        isPressingNumber = false
        var result = "0"
        secondNumber = Int(numberToDisplay) ?? 0
        if setOperand == "+" {
            result = String(firstNumber + secondNumber)
        } else if setOperand == "-" {
            result = String(firstNumber - secondNumber)
        } else if setOperand == "÷" {
            result = String(Double(firstNumber) / Double(secondNumber))
        } else if setOperand == "x" {
            result = String(firstNumber * secondNumber)
        }
        view.giveResult(result: result)
    }
}
