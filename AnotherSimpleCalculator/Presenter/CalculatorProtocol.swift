//
//  CalculatorProtocol.swift
//  AnotherSimpleCalculator
//
//  Created by Muhammad Rizqi Imani on 19/05/20.
//  Copyright © 2020 Qatros. All rights reserved.
//

import Foundation

//View -> Presenter
protocol ViewToPresenter {
    func numberPressed(number: String)
    func operandPressed(operand: String)
    func clearPressed()
    func equalPressed()
}

//View <- Presenter
protocol PresenterToView {
    func setPressedNumber(number: String)
    func cleared()
    func setPressedOperand()
    func giveResult(result: String)
//    func add(number: Int, result: Int)
}
